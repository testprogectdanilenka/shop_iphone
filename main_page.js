$.getJSON('goods.json', function(data) {

    const basket = localStorage.basket ? JSON.parse(localStorage.basket) : [];

    function checkBasket() {
        if((basket.length === 0) || (basket['value'] === 0)) {
            $('.basket').text('');
            $('.nav-basket span').removeClass('notification');
        } else {
            $('.basket').text(basket['value']);
            $('.nav-basket span').addClass('notification');
        };
    };
    checkBasket();

    $.each(data, function(index) {
        const itemPhone = $('<div>', {class: 'item-phone'}).attr('data-article', index);
        const itemPhoneLink = $('<a>', {class: 'item-phone-link flex-col'}).attr('href', this['htmlPage']);
        const itemPhoneImg = $('<img>', {class: 'item-phone-img'}).attr('src', this['imageAllColor']);
        const itemPhoneName = $('<p>', {class: 'item-phone-name'}).text(this['model']);
        itemPhoneImg.add(itemPhoneName).appendTo(itemPhoneLink);
        itemPhoneLink.appendTo(itemPhone);
        itemPhone.appendTo('.models-phone');
    });

    $('.item-phone').on('click', rememberArticle);
});

function rememberArticle() {
    const chosenArticle = {};
    chosenArticle.article = $(this).attr('data-article');
    localStorage.setItem('chosenArticle', JSON.stringify(chosenArticle));
};