const orderingModel = JSON.parse(localStorage.getItem('orderingModel'));
const basket = localStorage.basket ? JSON.parse(localStorage.basket) : [];

function emptyBin() {
    $('<div>', {class: 'bin-message', append: $('<p>', {text: 'В вашей корзине пока ничего нет'})
    .add($('<p>', {text: 'Корзина ждет, что ее наполнят. Желаем приятных покупок!'}))}).appendTo('.bin-wrap');
    $('.bin-table').remove();
};

function checkBasket() {
    if((basket.length === 0) || (basket['value'] === 0)) {
        $('.basket').text('');
        $('.nav-basket span').removeClass('notification');
        $('.bin-table').remove();
        emptyBin();
    } else {
        $('.basket').text(basket['value']);
        $('.nav-basket span').addClass('notification');
    };
};
checkBasket();

function isOrderingModel() {
    if((orderingModel !== null) || (orderingModel.length !== 0)) {
        createLeftBasket();
        createRightBasket();
    };
};
isOrderingModel();

function createLeftBasket() {
    const checkAllWrap = $('<div>', {class: 'checkAll-wrap'});
    const allGoods = $('<input>', {type: 'checkbox', id: 'allGoods'});
    const lableAllGoods = $('<label>', {for: 'allGoods'}).text('Выбрать все товары');
    allGoods.add(lableAllGoods).appendTo(checkAllWrap);
    checkAllWrap.appendTo('.table-left');
    
    makeItems();

    const buttonDelete = $('<button>', {class: 'goods-delete button', text: 'УДАЛИТЬ'});
    buttonDelete.appendTo('.table-left');
};

function makeItems() {
    const goodsTable = $('<div>', {class: 'table-goods'});
    goodsTable.appendTo('.table-left');

    $.each(orderingModel, function() {
        const div = $('<div>', {class:'goods-item flex-row'}).attr('data-article', this['art']);
        const input = $('<input>', {type: 'checkbox'}).attr('data-article', this['art']);
        const itemImg = $('<img>', {class: 'item-image', src: this['discription']['image']});
        const name = $('<p>', {text: this['discription']['model']});
        const memory = $('<p>', {text: this['discription']['memory']});
        const amountWrap = $('<div>', {class:'item-value flex-row'});
        const buttonReduce = $('<button>', {class: 'value-reduce', text: '-'});
        let itemValue = $('<p>', {class: 'value', text: this['value']});
        const buttonIncrease = $('<button>', {class: 'value-add', text: '+'});
        let cost = $('<p>', {text: Number(this['discription']['cost'])});
        let sum = $('<p>', {class: 'item-sum', text: (Number(this['discription']['cost']) * Number(this['value']))});

        buttonReduce.add(itemValue).add(buttonIncrease).appendTo(amountWrap);
        input.add(itemImg).add(name).add(memory).add(amountWrap).add(cost).add(sum).appendTo(div);
        div.appendTo(goodsTable);    
    });
};

function checkAllGoods() {
    if($('#allGoods').is(':checked')) {
        $('.goods-item input:checkbox').prop('checked', true);
    } else {
        $('.goods-item input:checkbox').prop('checked', false);
    };
};

$('#allGoods').on('click', checkAllGoods);

function deleteGoods() {
    const goodsItemChecked = $('.goods-item input:checked');

    let arrGoods = [];
    $.each(goodsItemChecked, function() {
        arrGoods.push($(this).attr('data-article'));
    });

    $.each(arrGoods, function(index, element) {

        $.each(orderingModel, function(index) {
            if(element === this['art']) {
                orderingModel.splice(index, 1);
                basket['value'] = basket['value'] - this['value'];
            };
        });
    });
    let chosedElement = $('.goods-item input:checked').parent();
    chosedElement.remove();

    localStorage.setItem('orderingModel', JSON.stringify(orderingModel));
    localStorage.setItem('basket', JSON.stringify(basket));
    checkBasket();
    getTotalSum();
};

$('.goods-delete').on('click', deleteGoods);

function clickVolume() {
    const itemModelChecked = $(this).parents('.goods-item');
    const article = $(itemModelChecked).attr('data-article');
    const itemValue = $(this).siblings('.value').text();
    let value = Number(itemValue);
    const className = this.className;

    $.each(orderingModel, function(index) {
        if(this['art'] === article) {

            if(className === 'value-add') {
                value++;
                this['value']++;
                basket['value']++;
            } else {
                value--;
                this['value']--;
                basket['value']--;
            };

            $('.basket').text(basket['value']);

            if(value === '0') {
                itemModelChecked.remove();
                orderingModel.splice(index, 1);
            };

            if(orderingModel.length === 0) {
                checkBasket();
            };

            $(itemModelChecked).children('.item-sum').text((Number(this['discription']['cost']) * Number(this['value'])));
        };
    });
    localStorage.setItem('orderingModel', JSON.stringify(orderingModel));
    localStorage.setItem('basket', JSON.stringify(basket));
    getTotalSum();

    $(this).siblings('.value').text(value);
};

function createRightBasket() {
    const ordering = $('<p>', {text:'Итого: '});
    let totalSum = $('<span>', {class:'total-sum'});
    const buttonOrder = $('<button>', {class: 'button-order button', type:'submit', text: 'ОФОРМИТЬ ЗАКАЗ'});
    totalSum.appendTo(ordering);
    ordering.add(buttonOrder).appendTo($('.table-right'));

    getTotalSum();
    createPersonalData();
};

function getTotalSum() {
    let summ = 0;

    $.each($('.goods-item .item-sum'), function() {
        summ += Number($(this).text());
    });

    $('.total-sum').text(`${summ} руб`);
};

function createPersonalData() {
    const wrapShading = $('<div>', {class: 'wrap-shading'});
    const wrapPersonalData = $('<div>', {class: 'wrap-personal-data flex-col'});
    const formPersonalData = $('<form>', {class: 'data-form flex-col'});
    const close = $('<div>', {class: 'data-close'});
    const legend = $('<legend>', {class: 'form-legend', text: 'Заполните персональные данные'});
    const itemName = $('<div>', {class: 'form-item flex-row'});
    const labelName = $('<label>', {text: 'Ваше имя', for: 'item-name'});
    const inputName = $('<input>', {id: 'item-name', type: 'text'});
    const itemAddress = $('<div>', {class: 'form-item flex-row'});
    const labelAddress = $('<label>', {text: 'Ваш адрес', for: 'item-address'});
    const inputAddress = $('<input>', {id: 'item-address', type: 'text'});
    const itemNumber = $('<div>', {class: 'form-item flex-row'});
    const labelNumber = $('<label>', {text: 'Ваш номер телефона', for: 'item-number'});
    const inputNumber = $('<input>', {id: 'item-number', type: 'text', placeholder: '+375(XX)XXX-XX-XX', mask:'+375 (99) 999-99-99'});
    const note = $('<p>', {class: 'note', text: '*имя должно содержать только буквы'});
    const inputSubmit = $('<input>', {class: 'form-submit button', value: 'Подтвердить заказ'});

    labelName.add(inputName).appendTo(itemName);
    labelAddress.add(inputAddress).appendTo(itemAddress);
    labelNumber.add(inputNumber).appendTo(itemNumber);

    legend.add(itemName).add(itemAddress).add(itemNumber).add(note).add(inputSubmit).appendTo(formPersonalData);
    close.add(formPersonalData).appendTo(wrapPersonalData);
    wrapPersonalData.appendTo(wrapShading);
    wrapShading.prependTo($('body'));   
    
    $('.wrap-shading').hide();    
};

function acceptOrdering() {
    $('.wrap-shading').show();
};

function checkValidity() {
    $('.message').remove();
    const inputName = $('#item-name').val();
    const inputAddress = $('#item-address').val();
    const inputNumber = $('#item-number').val();
    const regexp = new RegExp('^[а-яА-ЯёЁa-zA-Z]+$');

    if(inputName.length === 0 || inputAddress.length === 0 || inputNumber.length === 0) {
        showErrorEmpty();
    } else if(!regexp.test(inputName)) {
        showErrorNumber();
    } else {
        $('.data-form').children().remove();
        showСonfirmation();
        let linkMain = $('<a>', {class: 'link-main', href: './main_page.html', text: 'вернуться на главную страницу'});
        linkMain.appendTo($('.data-form'));
        
        while(orderingModel.length !== 0) {
            orderingModel.splice(0,1);
        };

        basket['value'] = 0;
        localStorage.setItem('orderingModel', JSON.stringify(orderingModel));
        localStorage.setItem('basket', JSON.stringify(basket));
        checkBasket();
    };
};

$('.form-submit').on('click', checkValidity);

function showErrorNumber() {
    $('.message-error').remove();

    const messageError = $('<p>', {class: 'message-error', text: 'Имя должно содержать только буквы'});
    messageError.appendTo($('.form-legend'));
};

function showErrorEmpty() {
    $('.message-error').remove();

    const messageError = $('<p>', {class: 'message-error', text: 'Все поля должны быть заполнены'});
    messageError.appendTo($('.form-legend'));
};

function showСonfirmation() {
    const messageСonfirmation = $('<p>', {class: 'message-confirmation', text: 'Спасибо за заказ! В ближайшее время с вами свяжется наш оператор.'});
    messageСonfirmation.appendTo($('.data-form'));
};

$('.button-order').on('click', acceptOrdering);
$('.value-add').on('click', clickVolume);
$('.value-reduce').on('click', clickVolume);

$('.data-close').on('click', function() {
    $('.wrap-shading').hide();    
});