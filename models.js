$.getJSON('goods.json', function(data) {

    const chosenArticle = JSON.parse(localStorage.getItem('chosenArticle'));
    const basket = localStorage.basket ? JSON.parse(localStorage.basket) : [];

    function checkBasket() {
        if((basket.length === 0) || (basket['value'] === 0)) {
            $('.basket').text('');
            $('.nav-basket span').removeClass('notification');
        } else {
            $('.basket').text(basket['value']);
            $('.nav-basket span').addClass('notification');
        };
    };
    checkBasket();
    
    let idColor;
    let idMemory;
    let that;

    const article = chosenArticle['article'];
    
    $.each(data, function(index, element) {

        if(index === article) {

            that = this;
            //Присвоила значение блоку discription при загрузке
            $('.size').text(`${this['diagonal']}"`);
            $('.camera').text(this['camera']);

            //Добавляю inputs рассцветок
            $.each(this['color'], function(index) {
                const colorItem = $('<div>', {class: 'color-item'});
                const input = $('<input>', {type: 'radio', name: 'color'}).attr('id', index);
                const imgColor = $('<img>').attr('src', this);
                const label = $('<label>', {class: 'label-color-item'}).attr('for', input.attr('id'));
                imgColor.appendTo(label);
                input.add(label).appendTo(colorItem);
                colorItem.appendTo('.options-color');
            });

            //Добавляю inputs памяти
            $.each(this['memory'], function(index) {
                const memoryItem = $('<div>', {class: 'memory-item'});
                const input = $('<input>', {type: 'radio', name: 'memory'}).attr('id', index);
                const label = $('<label>').attr('for', input.attr('id')).text(this);
                input.add(label).appendTo(memoryItem);
                memoryItem.appendTo('.options-memory');
            });
            
            //При загрузке первый input рассцветок - checked
            $.each($('.color-item input'), function(index, element) {
                if(index === 0) {
                    $(element).attr('checked', true);
                };

                if($(element).is(':checked')) {
                    idColor = $(element).attr('id');
                };
            });

            //При загрузке первый input памяти - checked
            $.each($('.memory-item input'), function(index, element) {
                if(index === 0) {
                    $(element).attr('checked', true);
                };

                if($(element).is(':checked')) {
                    idMemory = $(element).attr('id');
                };
            });
                
            function addParameters() {
                $.each(element, function(index) {  

                    if(index === idColor) {
                        $('.memory').text(this[idMemory]['memory']);
                        $('.summary-cost').text(this[idMemory]['cost']);
                        $('.model-img').attr('src', this[idMemory]['image']);
                        $('.model .discription-name').text(this[idMemory]['model']);
                    };
                });                
                $('.model .article').text(`${article}.${idColor}.${idMemory}`);
            };
            addParameters();

            $('.memory-item input').on('click', function() {
                idMemory = $(this).attr('id');
                addParameters();
            });

            $('.color-item input').on('click', function() {
                idColor = $(this).attr('id');
                addParameters();
            });
        };
    });

    function rememberModel() {

        const orderingModel = localStorage.orderingModel ? JSON.parse(localStorage.orderingModel) : [];
        const obj = {};
        let flag = false;
        const art = $('.model .article').text();
        const discription = that[idColor][idMemory];

        if(orderingModel.length !== 0) {

            $.each(orderingModel, function() {
                if(this['art'] === art) {
                    flag = true;
                };
            });
                
            if(flag) {
                $.each(orderingModel, function() {
                    if(this['art'] === art) {
                        this['value']++;
                    };
                });
            } else {
                obj.art = art;
                obj.discription = discription;
                obj.value = 1;
                orderingModel.push(obj);
            };
            localStorage.setItem('orderingModel', JSON.stringify(orderingModel));
        } else {
            obj.art = art;
            obj.discription = discription;
            obj.value = 1;
            orderingModel.push(obj);
            localStorage.setItem('orderingModel', JSON.stringify(orderingModel));
        };

        let countBin = Number($('.basket').text());
        countBin++;
        $('.basket').text(countBin);
        $('.nav-basket span').addClass('notification');

        const basket = {};
        basket.value = countBin;
        localStorage.setItem('basket', JSON.stringify(basket));
    };
    $('.summary-order').on('click', rememberModel);
});